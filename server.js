const express = require("express");

const cors = require("cors");

const app=express();



var corsOptions ={
    origin: "http://localhost:3000" // connect to front end if front end is running on port 3000
}

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({extended:true}));

const db = require("./app/models");

db.sequ.sync();

app.get("/", (req, res) => {
    res.json({ message: "Welcome to Fernando's page." });
  });

  require("./app/routes/entries.routes")(app);

  const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});