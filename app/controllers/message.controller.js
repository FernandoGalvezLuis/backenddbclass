const db = require("../models");  // db comes from DB.index.js which is exporting the connection to DB done with sequelize
//with standard info from db.config.js
const INCOMING_MESSAGE = db.incoming_message;
const Op = db.Sequelize.Op;

// Create and Save a new INCOMING_MESSAGE
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a INCOMING_MESSAGE
  const incoming_message = {
    name: req.body.name,
    email: req.body.email,
    message_content: req.body.message_content,
        // I won´t be needing to check wether something is done or not here published: req.body.published ? req.body.published : false
  };

  // Save INCOMING_MESSAGE in the database
  INCOMING_MESSAGE.create(incoming_message)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the INCOMING_MESSAGE."
      });
    });
};

// // Retrieve all INCOMING_MESSAGE from the database.
// exports.findAll = (req, res) => {
//   const ocupationName = req.query.ocupationName;
//   var condition = ocupationName ? { ocupationName: { [Op.like]: `%${ocupationName}%` } } : null;

//   ENTRY.findAll({ where: condition })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving all of ENTRY."
//       });
//     });
// };

// // Find a single INCOMING_MESSAGE with an id assuming that in MY case "id" is automatically created in our DB
// exports.findOne = (req, res) => {
//   const id = req.params.id;

//   ENTRY.findById(id)
//     .then(data => {
//       if (data) {
//         res.send(data);
//       } else {
//         res.status(404).send({
//           message: `Cannot find ENTRY with id=${id}.`
//         });
//       }
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Error retrieving ENTRY with id=" + id
//       });
//     });
// };

// // Update a INCOMING_MESSAGE by the id in the request
// exports.update = (req, res) => {
//   const id = req.params.id;

//   ENTRY.update(req.body, {
//     where: { id: id }
//   })
//     .then(num => {
//       if (num == 1) {
//         res.send({
//           message: "ENTRY was updated successfully."
//         });
//       } else {
//         res.send({
//           message: `Cannot update ENTRY with id=${id}. Maybe Tutorial was not found or req.body is empty!`
//         });
//       }
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Error updating ENTRY with id=" + id
//       });
//     });
// };

// // Delete a INCOMING_MESSAGE with the specified id in the request
// exports.delete = (req, res) => {
//   const id = req.params.id;

//   Tutorial.destroy({
//     where: { id: id }
//   })
//     .then(num => {
//       if (num == 1) {
//         res.send({
//           message: "Entry was deleted successfully!"
//         });
//       } else {
//         res.send({
//           message: `Cannot delete ENTRY with id=${id}. Maybe ENTRY was not found!`
//         });
//       }
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Could not delete ENTRY with id=" + id
//       });
//     });
// };

// // Delete all INCOMING_MESSAGE from the database.
// exports.deleteAll = (req, res) => {
//   Tutorial.destroy({
//     where: {},
//     truncate: false
//   })
//     .then(nums => {
//       res.send({ message: `${nums} All ENTRY were deleted successfully!` });
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while removing all entries."
//       });
//     });
// };