module.exports = app => {
    const incoming_message = require("../controllers/message.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", incoming_message.create);
  
    // // Retrieve all Tutorials
    // router.get("/", entries.findAll);
  
    // // Retrieve all published Tutorials
    // //router.get("/published", tutorials.findAllPublished);
  
    // // Retrieve a single Tutorial with id
    // router.get("/:id", entries.findOne);
  
    // // Update a Tutorial with id
    // router.put("/:id", entries.update);
  
    // // Delete a Tutorial with id
    // router.delete("/:id", entries.delete);
  
    // // Delete all Tutorials
    // router.delete("/", entries.deleteAll);
  
    app.use('/api/incoming_message', router);
  };