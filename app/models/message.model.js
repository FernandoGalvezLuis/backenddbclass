module.exports = (sequ, Sequelize) => {
    //we are going to define ENTRY that will contain or retrieve all the elements of the table where it will be inserted or from it
    //will be retrieved
    const incoming_message = sequ.define(
      "incoming_message" /**name of table within our Data Base*/
    , {
        message_id: {
          type: Sequelize.INTEGER 
        },
        name: {
          type: Sequelize.STRING
        },
        email: {
          type: Sequelize.STRING
        },
        message_content: {
          type: Sequelize.STRING
        },
        
    });
  
    return incoming_message;
  };