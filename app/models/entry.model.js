module.exports = (sequ, Sequelize) => {
    //we are going to define ENTRY that will contain or retrieve all the elements of the table where it will be inserted or from it
    //will be retrieved
    const entry = sequ.define(
      "Resume_Entry" /**name of table within our Data Base*/
    , {
        id: {
          type: Sequelize.INTEGER 
        },
        ocupationName: {
          type: Sequelize.STRING
        },
        startingEndingDate: {
          type: Sequelize.STRING
        },
        companyNameLocation: {
          type: Sequelize.STRING
        },
        description: {
          type: Sequelize.STRING
        }
    });
  
    return entry;
  };